$(document).ready(() => {
    $('#sid-form').show();
    $('#otp-form').hide();

    $('#otpLogin').on('change', () => {
        if ($('#otpLogin').is(':checked')) {
            $('#sid-form').hide();
            $('#otp-form').show();
            $("#otpLogin").prop("checked", true);
            $("#ssidLogin").prop("checked", false);
        }
    })

    $('#ssidLogin').on('change', () => {
        if ($('#ssidLogin').is(':checked')) {
            $('#sid-form').show();
            $('#otp-form').hide();
            $("#ssidLogin").prop("checked", true);
            $("#otpLogin").prop("checked", false);
        }
    })
})

//  booking flow Js
// $(document).ready(() => {
//     $('.card-header .btn-link').on('click', () => {
//         $(this).closest('.card-header').addClass('card-header-active')
//     })
// })